# Custom R Dockerfile

R Docker image nigthly build :  
* [R-base-biosp](r-base-biosp) : r-base + dependencies packages for R packaging pipeline
* [R-devel-biosp](r-devel-biosp) : R devel version + dependencies packages for R packaging pipeline

## CI/CD

In your __.gitlab-ci.yml__ file use tags to select the docker image.

```yaml
tags:
    - "r-base-biosp"
```
```yaml
tags:
    - "r-devel-biosp"
```


## Author 

* Jean-François Rey <jean-francois.rey at inra.fr>

## License

[LICENSE](LICENSE)
